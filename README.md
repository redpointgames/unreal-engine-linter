# Unreal Engine Linter

This is a release of the C++ linter used by [EOS Online Subsystem](http://redpointgames.gitlab.io/eos-online-subsystem/).

It currently only works for plugins, but you should be able to adapt it to work on `.uproject` files without much effort.

## Usage

You can build and run it from the command line:

```bat
dotnet build /p:Configuration=Release /p:Platform=x64
UnrealEngineLinter\bin\x64\Release\netcoreapp3.1\UnrealEngineLinter.exe -p C:\Path\To\Your\Project -n YourPluginName -e 4.26
```

You can set it up in GitLab with a build job like this (assuming this repository has been added as a submodule under `Tooling/UnrealEngineLinter`):

```yaml
lint:
  stage: build
  tags:
    - windows
  script:
    - cd Tooling\UnrealEngineLinter
    - dotnet build /p:Configuration=Release /p:Platform=x64
    - UnrealEngineLinter\bin\x64\Release\netcoreapp3.1\UnrealEngineLinter.exe -p ..\.. -n YourPluginName -e 4.26
  interruptible: true
```

## License

```
MIT License

Copyright (c) 2021 June Rhodes

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
```