﻿// Copyright June Rhodes 2021. MIT Licensed.

using ClangSharp;
using ClangSharp.Interop;
using System.Threading.Tasks;

namespace UnrealEngineLinter
{
    public interface ILintRule
    {
        Task<bool> Lint(Cursor current, ILogger logger, CXFile file, uint line, uint column);
    }
}
