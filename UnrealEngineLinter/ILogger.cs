﻿// Copyright June Rhodes 2021. MIT Licensed.

using System.Threading.Tasks;

namespace UnrealEngineLinter
{
    public enum LogLevel
    {
        Success,
        Error,
        Warning,
        Info,
        Verbose,
        Debug,
    };

    public interface ILogger
    {
        Task Log(LogLevel level, string message);
    }
}
