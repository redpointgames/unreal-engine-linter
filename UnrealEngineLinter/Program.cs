﻿// Copyright June Rhodes 2021. MIT Licensed.

using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using ClangSharp;
using ClangSharp.Interop;
using CommandLine;
using Microsoft.Build.Locator;
using Microsoft.Win32;
using NCode.ReparsePoints;
using net.r_eg.MvsSln;
using net.r_eg.MvsSln.Core;
using static System.Environment;

namespace UnrealEngineLinter
{
    public class Program : ILogger
    {
        public class Options
        {
            [Option('p', "path", Required = true, HelpText = "Path to the directory that contains the plugin directory (named with --name).")]
            public string ProjectPath { get; set; }

            [Option('n', "name", Required = true, HelpText = "Name of the plugin (the .uplugin file without extension).")]
            public string PluginName { get; set; }

            [Option('e', "engine-version", Required = true, HelpText = "The version number of the engine, like '4.25'.")]
            public string EngineVersion { get; set; }

            [Option('v', "verbose", HelpText = "Enable verbose output.")]
            public bool EnableVerbose { get; set; }

            [Option('d', "debug", HelpText = "Enable debug output.")]
            public bool EnableDebug { get; set; }
        }

        private SemaphoreSlim consoleSem = new SemaphoreSlim(1);
        private Options o;
        private List<ILintRule> rules;

        public async Task Log(LogLevel level, string message)
        {
            if (level == LogLevel.Success)
            {
                await consoleSem.WaitAsync();
                try
                {
                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine(message);
                    Console.ResetColor();
                }
                finally
                {
                    consoleSem.Release();
                }
                return;
            }

            if (level == LogLevel.Error)
            {
                await consoleSem.WaitAsync();
                try
                {
                    Console.ForegroundColor = ConsoleColor.Red;
                    Console.WriteLine(message);
                    Console.ResetColor();
                }
                finally
                {
                    consoleSem.Release();
                }
                return;
            }

            if (level == LogLevel.Warning)
            {
                await consoleSem.WaitAsync();
                try
                {
                    Console.ForegroundColor = ConsoleColor.Yellow;
                    Console.WriteLine(message);
                    Console.ResetColor();
                }
                finally
                {
                    consoleSem.Release();
                }
                return;
            }

            if (level == LogLevel.Verbose && !o.EnableVerbose)
            {
                return;
            }

            if (level == LogLevel.Debug && !o.EnableDebug)
            {
                return;
            }

            await consoleSem.WaitAsync();
            try
            {
                Console.WriteLine(message);
            }
            finally
            {
                consoleSem.Release();
            }
        }

        static async Task Main(string[] args)
        {
            MSBuildLocator.RegisterDefaults();

            var o = Parser.Default.ParseArguments<Options>(args).Value;
            if (o == null)
            {
                Environment.Exit(1);
                return;
            }

            var program = new Program(o);
            await program.Run();
        }

        private Program(Options InO)
        {
            this.o = InO;

            // Load all rules from assembly.
            this.rules = this.GetType().Assembly.GetTypes().Where(x => x.IsAssignableFrom(typeof(ILintRule)) && !x.IsAbstract && x.IsClass).Select(x => (ILintRule)Activator.CreateInstance(x)).ToList();
        }

        private async Task Run()
        {
            if (!Directory.Exists(Path.Combine(o.ProjectPath, o.PluginName)))
            {
                await Log(LogLevel.Error, $"Plugin does not exist at path: {Path.Combine(o.ProjectPath, o.PluginName)}");
                Environment.Exit(1);
                return;
            }

            // Locate Unreal Engine.
            await Log(LogLevel.Info, "Locating Unreal Engine...");
            var unrealEnginePath = Path.Combine(Environment.GetEnvironmentVariable("PROGRAMFILES"), "Epic Games", "UE_" + o.EngineVersion);
            RegistryKey engineKey = Registry.LocalMachine.OpenSubKey(@$"SOFTWARE\EpicGames\Unreal Engine\{o.EngineVersion}");
            if (engineKey != null)
            {
                unrealEnginePath = (string)engineKey.GetValue("InstalledDirectory");
            }
            if (!Directory.Exists(unrealEnginePath))
            {
                await Log(LogLevel.Error, $"Unreal Engine {o.EngineVersion} could not be found");
                Environment.Exit(1);
                return;
            }

            // Generate host project structure.
            await Log(LogLevel.Info, "Generating host project structure...");
            var hostProjectDirectory = Path.Combine(o.ProjectPath, "LINT");
            Directory.CreateDirectory(hostProjectDirectory);
            File.WriteAllText(Path.Combine(hostProjectDirectory, "HostProject.uproject"), $@"
{{
    ""FileVersion"": 3,
    ""Plugins"": [
        {{
            ""Name"": ""{o.PluginName}"",
            ""Enabled"": true
        }}
    ]
}}
"
            );
            Directory.CreateDirectory(Path.Combine(hostProjectDirectory, "Plugins"));
            var provider = ReparsePointFactory.Provider;
            if (!Directory.Exists(Path.Combine(hostProjectDirectory, "Plugins", o.PluginName)))
            {
                provider.CreateLink(Path.Combine(hostProjectDirectory, "Plugins", o.PluginName), Path.Combine(o.ProjectPath, o.PluginName), LinkType.Junction);
            }
            Directory.CreateDirectory(Path.Combine(hostProjectDirectory, "Source"));
            File.WriteAllText(Path.Combine(hostProjectDirectory, "Source", "HostProject.Target.cs"), @"
using UnrealBuildTool;
using System.Collections.Generic;
public class HostProjectTarget : TargetRules
{
    public HostProjectTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Game;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.AddRange(new string[] { ""HostProject"" });
    }
}
"
);
            File.WriteAllText(Path.Combine(hostProjectDirectory, "Source", "HostProjectEditor.Target.cs"), @"
using UnrealBuildTool;
using System.Collections.Generic;
public class HostProjectEditorTarget : TargetRules
{
    public HostProjectEditorTarget(TargetInfo Target) : base(Target)
    {
        Type = TargetType.Editor;
        DefaultBuildSettings = BuildSettingsVersion.V2;
        ExtraModuleNames.AddRange(new string[] { ""HostProjectEditor"" });
    }
}
"
);
            Directory.CreateDirectory(Path.Combine(hostProjectDirectory, "Source", "HostProject"));
            Directory.CreateDirectory(Path.Combine(hostProjectDirectory, "Source", "HostProjectEditor"));
            var depNames = string.Join(",", new[] { o.PluginName }.Select(x => "\"" + x + "\""));
            File.WriteAllText(Path.Combine(hostProjectDirectory, "Source", "HostProject", "HostProject.Build.cs"), $@"
using UnrealBuildTool;
public class HostProject : ModuleRules
{{
    public HostProject(ReadOnlyTargetRules Target) : base(Target)
    {{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PublicDependencyModuleNames.AddRange(new string[] {{
            ""Core"",
            ""CoreUObject"",
            ""Engine"",
            {depNames}
        }});
    }}
}}
"
);
            File.WriteAllText(Path.Combine(hostProjectDirectory, "Source", "HostProjectEditor", "HostProjectEditor.Build.cs"), $@"
using UnrealBuildTool;
public class HostProjectEditor : ModuleRules
{{
    public HostProjectEditor(ReadOnlyTargetRules Target) : base(Target)
    {{
        PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;
        PublicDependencyModuleNames.AddRange(new string[] {{
            ""Core"",
            ""CoreUObject"",
            ""Engine"",
            {depNames}
        }});
    }}
}}
"
);
            File.WriteAllText(Path.Combine(hostProjectDirectory, "Source", "HostProject", "HostProjectModule.cpp"), @"
#include ""CoreMinimal.h""
#include ""Logging/LogMacros.h""
#include ""Modules/ModuleManager.h""
class FHostProjectModule : public IModuleInterface
{
};
IMPLEMENT_PRIMARY_GAME_MODULE(FHostProjectModule, HostProject, ""HostProject"");
"
);
            File.WriteAllText(Path.Combine(hostProjectDirectory, "Source", "HostProjectEditor", "HostProjectEditorModule.cpp"), @"
#include ""CoreMinimal.h""
#include ""Logging/LogMacros.h""
#include ""Modules/ModuleManager.h""
class FHostProjectEditorModule : public IModuleInterface
{
};
IMPLEMENT_MODULE(FHostProjectEditorModule, HostProjectEditor);
"
);

            // Generate Visual Studio project files for host project.
            await Log(LogLevel.Info, "Generating Visual Studio projects...");
            var startInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(unrealEnginePath, "Engine", "Binaries", "DotNET", "UnrealBuildTool.exe"),
                ArgumentList = {
                    "-NoMutex",
                    "-projectfiles",
                    $"-project={Path.GetFullPath(hostProjectDirectory)}\\HostProject.uproject",
                    "-rocket",
                    "-progress"
                },
                UseShellExecute = false,
                WorkingDirectory = hostProjectDirectory,
            };
            var process = Process.Start(startInfo);
            process.WaitForExit();
            if (process.ExitCode != 0)
            {
                await Log(LogLevel.Error, $"Could not generate Visual Studio project files (UBT exited with non-zero exit code)");
                Environment.Exit(1);
                return;
            }

            // Run UnrealHeaderTool to create all the .generated.h files we expect.
            await Log(LogLevel.Info, "Generating header files...");
            startInfo = new ProcessStartInfo
            {
                FileName = Path.Combine(unrealEnginePath, "Engine", "Binaries", "DotNET", "UnrealBuildTool.exe"),
                ArgumentList = {
                    "-NoMutex",
                    "Development",
                    "Win64",
                    $"-project={Path.GetFullPath(hostProjectDirectory)}\\HostProject.uproject",
                    "-TargetType=Editor",
                    "-progress",
                    "-NoEngineChanges",
                    "-NoHotReloadFromIDE",
                    // The magic flag that makes it not actually compile source code.
                    "-SkipBuild",
                },
                UseShellExecute = false,
                WorkingDirectory = hostProjectDirectory,
            };
            process = Process.Start(startInfo);
            process.WaitForExit();
            if (process.ExitCode != 0)
            {
                await Log(LogLevel.Error, $"Could not generate Visual Studio project files (UBT exited with non-zero exit code)");
                Environment.Exit(1);
                return;
            }

            if (File.Exists(clangTidyPath))
            {
                await Log(LogLevel.Info, "Lint will also run clang-tidy for performance checks.");
            }
            else
            {
                await Log(LogLevel.Warning, "Lint WILL NOT RUN CLANG-TIDY. It could not be found at: " + clangTidyPath + " . Ensure VS is up to date and has the required workload installed!");
            }

            // Run the linting process.
            await Log(LogLevel.Info, "Starting lint...");
            if (!await LoadSlnAndLint(Path.Combine(hostProjectDirectory, "HostProject.sln"), o))
            {
                await Log(LogLevel.Error, $"One or more lint errors were reported");
                Environment.Exit(1);
                return;
            }
            await Log(LogLevel.Success, $"Linting passed");
        }

        public class MyEnv : IsolatedEnv, IEnvironment, IDisposable
        {
            public MyEnv(ISlnResult data)
                : base(data)
            {

            }

            protected override Microsoft.Build.Evaluation.Project Load(string path, IDictionary<string, string> properties)
            {
                // TODO: Fix this to use "vswhere" to locate the VC++ files.
                properties.Add("VCTargetsPath", @"C:\Program Files (x86)\Microsoft Visual Studio\2019\Community\MSBuild\Microsoft\VC\v160\");
                return base.Load(path, properties);
            }

            protected override void Dispose(bool disposing) => base.Dispose(disposing);
        }

        static string ResolvePath(string projectPath, string candidatePath)
        {
            if (Path.IsPathRooted(candidatePath))
            {
                return candidatePath;
            }

            var absolutePath = Path.Combine(projectPath, candidatePath);
            if (Directory.Exists(absolutePath) || File.Exists(absolutePath))
            {
                return Path.GetFullPath(absolutePath);
            }

            return null;
        }

        SemaphoreSlim taskSem = new SemaphoreSlim(Math.Max(Environment.ProcessorCount - 2, 4));

        async Task<bool> LoadSlnAndLint(string slnPath, Options options)
        {
            using (var sln = new Sln(slnPath, SlnItems.Projects | SlnItems.SolutionConfPlatforms
                                | SlnItems.ProjectConfPlatforms))
            {
                IEnvironment env = new MyEnv(sln.Result);
                env.LoadMinimalProjects();

                var project = env.GetOrLoadProject(
                    sln.Result.ProjectItems.FirstOrDefault(x => x.name != "UE4"));

                // Compute the common include paths.
                var includePathsRaw = project.GetPropertyValue("IncludePath").Split(";");
                var includePathsResolved = includePathsRaw.Select(x => ResolvePath(project.DirectoryPath, x)).Where(x => x != null).ToList();
                var includePaths = includePathsResolved.Select(x => "--include-directory=" + x).ToArray();

                // For each ClCompile entry, go and perform the AST linting.
                CXIndex index = CXIndex.Create();

                var tasks = new List<Task<bool>>();
                var clCompiles = project.GetItems("ClCompile").ToList();
                fileIndexGlobal = 1;
                foreach (var compile in clCompiles)
                {
                    tasks.Add(Task.Run(async () =>
                    {
                        await taskSem.WaitAsync();
                        try
                        {
                            try
                            {
                                return await ClangLint(clCompiles.Count, index, project.DirectoryPath, ResolvePath(project.DirectoryPath, compile.EvaluatedInclude), compile, includePaths, options);
                            }
                            catch (Exception ex)
                            {
                                await Log(LogLevel.Error, $"exception: {ex.ToString()}");
                                return false;
                            }
                        }
                        finally
                        {
                            taskSem.Release();
                        }
                    }));
                }
                await Task.WhenAll(tasks.ToArray());
                return tasks.All(x => x.Result);
            }
        }

        SemaphoreSlim fileIndexSem = new SemaphoreSlim(1);
        int fileIndexGlobal = 1;
        static string clangTidyPath = Path.Combine(
                Environment.GetFolderPath(SpecialFolder.ProgramFilesX86),
                "Microsoft Visual Studio", "2019", "Community", "VC", "Tools", "Llvm", "bin", "clang-tidy.exe");

        async Task<bool> ClangLint(int fileTotal, CXIndex index, string projectDirectoryPath, string filePath, Microsoft.Build.Evaluation.ProjectItem clCompile, string[] includePaths, Options options)
        {
            if (filePath == null)
            {
                return true;
            }

            await fileIndexSem.WaitAsync();
            int fileIndex = fileIndexGlobal++;
            await Log(LogLevel.Info, $"[{fileIndex}/{fileTotal}] {filePath}");
            fileIndexSem.Release();

            if (File.ReadAllText(filePath).Contains("// @nolint"))
            {
                // This file is explicitly excluded from linting.
                return true;
            }

            var additionalIncludePathsRaw = clCompile.GetMetadataValue("AdditionalIncludeDirectories").Split(";");
            var additionalIncludePathsResolved = additionalIncludePathsRaw.Select(x => ResolvePath(projectDirectoryPath, x)).Where(x => x != null).ToList();
            var additionalIncludePaths = additionalIncludePathsResolved.Select(x => "--include-directory=" + x).ToArray();
            var forcedIncludeFiles = clCompile.GetMetadataValue("ForcedIncludeFiles").Split(";").Where(x => File.Exists(x) && !x.Contains("SharedPCH.")).Select(x => "--include=" + x).ToList();

            if (forcedIncludeFiles.Count == 0)
            {
                await Log(LogLevel.Warning, $"[{fileIndex}/{fileTotal}] warning: skipping {filePath} because it has no definitions file (module probably not built in this target!)");
                return true;
            }

            await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] {additionalIncludePaths.Count()} included directories, {forcedIncludeFiles.Count} included files");

            var commandArgs = includePaths.Concat(additionalIncludePaths).Concat(forcedIncludeFiles).Concat(new string[] {
                "-fdiagnostics-format=msvc",
                "-Wno-unused-private-field",
                "-Wno-tautological-compare",
                "-Wno-undefined-bool-conversion",
                "-Wno-unused-local-typedef",
                "-Wno-inconsistent-missing-override",
                "-Wno-undefined-var-template",
                "-Wno-unused-lambda-capture",
                "-Wno-unused-variable",
                "-Wno-unused-function",
                "-Wno-switch",
                "-Wno-unknown-pragmas",
                "-Wno-invalid-offsetof",
                "-Wno-gnu-string-literal-operator-template",
                "-Wno-nonportable-include-path",
                "-Wno-ignored-attributes",
                "-Wno-microsoft-unqualified-friend",
                "-Wno-implicit-exception-spec-mismatch",
                "/Zc:dllexportInlines-",
                "-std=c++17",
                "-DUNREAL_CODE_ANALYZER=1",
            }).ToArray();

            foreach (var commandLineArg in commandArgs)
            {
                await Log(LogLevel.Debug, commandLineArg);
            }

            if (File.Exists(clangTidyPath))
            {
                var clangTidyStopwatch = Stopwatch.StartNew();
                await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] clang-tidy: starting");

                var startInfo = new ProcessStartInfo
                {
                    FileName = clangTidyPath,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                };
                startInfo.ArgumentList.Add(filePath);
                startInfo.ArgumentList.Add("-checks=-*,performance-*");
                startInfo.ArgumentList.Add("--");
                foreach (var commandLineArg in commandArgs)
                {
                    startInfo.ArgumentList.Add(commandLineArg);
                }
                var process = Process.Start(startInfo);

                await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] clang-tidy: finished ({clangTidyStopwatch.Elapsed})");

                await Task.WhenAll(new Task[]
                    {
                        Task.Run(async () =>
                        {
                            var stdout = await process.StandardOutput.ReadToEndAsync();

                            foreach (var line in stdout.Split('\n'))
                            {
                                if (line.Contains("warning:") && line.Contains("[performance-"))
                                {
                                    await Log(LogLevel.Error, line.Trim().Replace("warning:", "error CLTIDY:"));
                                }
                            }
                        }),
                        Task.Run(async() =>
                        {
                            var stderr = await process.StandardError.ReadToEndAsync();
                        })
                    });
            }

            await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] translation unit parse: starting");

            var stopwatch = Stopwatch.StartNew();
            var translationUnitError = CXTranslationUnit.TryParse(index,
                filePath,
                commandArgs,
                Array.Empty<CXUnsavedFile>(),
                CXTranslationUnit.DefaultEditingOptions |
                CXTranslationUnit_Flags.CXTranslationUnit_IgnoreNonErrorsFromIncludedFiles,
                out CXTranslationUnit handle);
            var skipProcessing = false;
            stopwatch.Stop();

            await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] translation unit parse: finished ({stopwatch.Elapsed})");

            if (translationUnitError != CXErrorCode.CXError_Success)
            {
                await Log(LogLevel.Error, $"[{fileIndex}/{fileTotal}] fatal error while processing");
                skipProcessing = true;
            }
            else if (handle.NumDiagnostics != 0)
            {
                var errorCount = 0;
                for (uint i = 0; i < handle.NumDiagnostics; ++i)
                {
                    using var diagnostic = handle.GetDiagnostic(i);

                    if (diagnostic.Severity == CXDiagnosticSeverity.CXDiagnostic_Error || diagnostic.Severity == CXDiagnosticSeverity.CXDiagnostic_Fatal)
                    {
                        errorCount++;
                    }

                    await Log(LogLevel.Error, diagnostic.Format(CXDiagnostic.DefaultDisplayOptions).ToString());
                }
                if (errorCount > 0)
                {
                    skipProcessing = true;
                }
            }

            if (skipProcessing)
            {
                return false;
            }

            await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] translation unit iteration: started");

            using (var translationUnit = TranslationUnit.GetOrCreate(handle))
            {
                if (translationUnit == null)
                {
                    await Log(LogLevel.Error, $"[{fileIndex}/{fileTotal}] error: no translation unit");
                    return false;
                }

                Stack<Cursor> stackCursor = new Stack<Cursor>();
                Stack<int> stackChildIndex = new Stack<int>();
                stackCursor.Push(translationUnit.TranslationUnitDecl);
                stackChildIndex.Push(0);
                var childIndex = 0;
                var hasErrors = false;
                int processedElements = 0;
                while (stackCursor.Count > 0)
                {
                    var parent = stackCursor.Peek();
                    var parentChildCount = parent.CursorChildren.Count;

                    // Check if we have run out of children.
                    if (childIndex >= parentChildCount)
                    {
                        stackCursor.Pop();
                        childIndex = stackChildIndex.Pop() + 1;
                        continue;
                    }

                    // Get the current element.
                    var current = parent.CursorChildren[childIndex];

                    processedElements++;
                    if (processedElements % 1000 == 0)
                    {
                        await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] translation unit iteration: processed {processedElements} elements");
                    }

                    // Ignore these elements entirely.
                    if (!current.Extent.Start.IsFromMainFile)
                    {
                        childIndex++;
                        continue;
                    }

                    current.Location.GetFileLocation(out CXFile file, out uint line, out uint column, out uint offset);

                    // Evaluate linter rules.
                    foreach (var rule in this.rules)
                    {
                        if (await rule.Lint(current, this, file, line, column))
                        {
                            hasErrors = true;
                        }
                    }

                    // If we have children, push the current element onto
                    // the stack for later, and then start processing children.
                    if (current.CursorChildren.Count > 0)
                    {
                        stackCursor.Push(current);
                        stackChildIndex.Push(childIndex);
                        childIndex = 0;
                    }
                    else
                    {
                        childIndex++;
                    }
                }

                await Log(LogLevel.Verbose, $"[{fileIndex}/{fileTotal}] translation unit iteration: finished");

                return !hasErrors;
            }
        }
    }
}
