﻿// Copyright June Rhodes 2021. MIT Licensed.

using ClangSharp;
using ClangSharp.Interop;
using System.Threading.Tasks;

namespace UnrealEngineLinter.Rules
{
    public class EOSPointersAndStructuresAreZeroInitialized : ILintRule
    {
        public async Task<bool> Lint(Cursor current, ILogger logger, CXFile file, uint line, uint column)
        {
            if (current is VarDecl varDecl)
            {
                if (varDecl.CursorParent is DeclStmt)
                {
                    if (varDecl.Type.ToString().StartsWith("EOS_"))
                    {
                        if (!varDecl.HasInit)
                        {
                            if (varDecl.Type.TypeClass == CX_TypeClass.CX_TypeClass_Pointer)
                            {
                                await logger.Log(LogLevel.Error, $"{file.Name}({line},{column}): error EOS001: Missing initializer for an EOS pointer. Use '{varDecl.Type.ToString()} {varDecl.Spelling} = nullptr' to initialize it to the zero value correctly.");
                            }
                            else
                            {
                                await logger.Log(LogLevel.Error, $"{file.Name}({line},{column}): error EOS002: Missing initializer for an EOS structure. Use '{varDecl.Type.ToString()} {varDecl.Spelling} = {{}}' to initialize it to the zero value correctly.");
                            }
                            return true;
                        }
                        else if (varDecl.Init.StmtClass == CX_StmtClass.CX_StmtClass_CXXConstructExpr && varDecl.Init.CursorKind == CXCursorKind.CXCursor_CallExpr)
                        {
                            await logger.Log(LogLevel.Error, $"{file.Name}({line},{column}): error EOS003: Default initializer for an EOS structure. Use '{varDecl.Type.ToString()} {varDecl.Spelling} = {{}}' to initialize it to the zero value correctly.");
                        }
                    }
                }
            }
            return false;
        }
    }
}
